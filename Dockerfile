FROM greyltc/archlinux
MAINTAINER Marco Schlicht (marcoschlicht@onlinehome.de)

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -S make git curl wget python python-pip mlocate jre10-openjdk-headless dia libreoffice pandoc
RUN pacman --noconfirm -S texlive-core texlive-bibtexextra texlive-langextra texlive-fontsextra texlive-latexextra biber
RUN pacman --noconfirm -S terminus-font otf-fira-code fontconfig

# Ensure UTF-8 lang and locale
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
	locale-gen
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'
ENV PATH="/usr/bin/vendor_perl:${PATH}"
