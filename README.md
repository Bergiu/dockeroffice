# DockerOffice

This Docker Image contains some useful tools for building Office documents.

## Docker
A compiled version of this image is available at:
[hub.docker.com/r/bergiu/dockeroffice/](https://hub.docker.com/r/bergiu/dockeroffice/)

To use it just set this image as your base image in your Dockerfile:
- `FROM bergiu/dockeroffice`

### Upload your own version
If you wan't to make your own version you can compile it with this command:
- `docker build -t yourname/dockeroffice:latest .`

Then run it and test your changes:
- `docker run -it --rm yourname/dockeroffice`

Upload it to docker hub:
- First login to [hub.docker.com](https://hub.docker.com)
- Then create a new repository called dockeroffice
- Then push it:
	- `docker push yourname/dockeroffice`

## Installed tools:
- make
	- Build your project
- git
	- Get resources from github/gitlab
- dia
	- Make Diagrams and export them as png
- libreoffice
	- Make Documents and export them as pdf
- grip, wkhtmltopdf
	- Convert Markdown to pdf
- latexmk
	- Compile latex documents

## Example:
You can for example make a README.md, then an latex project. For your latex project you need diagrams. These diagrams can be made by dia and libreoffice. Then you can export them to png and pdf and insert it to your latex file.

I have also added some examples to the example folder.

- `.gitlab-ci.yml`
	- This file shows how to use the dockeroffice image to create an ci pipeline that builds your projects and adds the result as artifact.
- `makefile`
	- You can use this example makefile to compile dia, libreoffice and tex documents.
	- Just modify this makefile and put it in your project with the `.gitlab-ci.yml` file. Then your project will be build automatically if you push it to your master branch.
- `Dockerfile`
	- How you can expand the base image with your new software that you need (here gimp and inkscape).
